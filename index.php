<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Weather</title>
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    </head>
    <body>

        
            <div id="left">
            </div>
            <div id="right">
            </div>
               <div id="overlay"> 
            <div id="h1">
                   
                <?php
                $URL = "https://samples.openweathermap.org/data/2.5/weather?q=Emmen&appid=3a1feb994d5c7fe618295efd7a0247fc";
                
                $json = json_decode(file_get_contents($URL));
                $TEMP = $json->main->temp;
                $WEATHER = $json->weather[0]->main;
                
                function kelvin_to_celsius($given_value) {
                    $celsius = $given_value - 273.15;
                    return $celsius;
                }
                
                $TEMP = kelvin_to_celsius($TEMP);
                $TEMP= round($TEMP, 0);
                echo "<h1>$TEMP °C</h1><br>";
                echo "<h2>weather: $WEATHER</h2>";
               
                ?>
            </div>
        </div>
    </body>
</html>